function dynamicImport (selector, file) {
  if (document.contains(document.querySelector(selector))) {
    import('./modules/' + file + '.js').then({})
  }
}

dynamicImport('.accordion', 'accordion')
dynamicImport('#selector', 'selector')
dynamicImport('.video-element', 'videos')
