class JSSelector {
    /**
     * @param (string) selector
     */
    constructor (selector) {
        this.selector = document.querySelector(selector)
        this.buttons = Array.from(this.selector.querySelectorAll('button'))
        this.objects = Array.from(this.selector.querySelectorAll('.object'))

        this.onClickButton()
    }

    /**
     * Event Listener: click on a button
     */
    onClickButton () {
        this.buttons.forEach((button) => {
            button.addEventListener('click', (e) => {
                e.preventDefault()

                this.addActiveClass(button)
                this.displayElements(button)
            })
        })
    }

    /**
     * Retire 'active' on others buttons and give 'active' class ont the clicked button
     */
    addActiveClass (button) {
        this.buttons.forEach((button) => {
            button.classList.remove('active')
        })
        button.classList.add('active')
    }

    /**
     * Display corresponding elements and ihde others
     */
    displayElements (button) {
        let target = button.getAttribute('target')
        this.objects.forEach((object) => {
            if (target === 'all' || object.classList.contains(target)) {
                object.style.visibility = 'visible'
                object.style.opacity = 1
            } else {
                object.style.visibility = 'visible'
                object.style.opacity = 0
            }
        })
    }
}

new JSSelector('#selector')
