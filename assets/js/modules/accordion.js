class Accordion {
  /**
   * @param (string) selector
   */
  constructor (selector) {
    this.accordions = document.querySelectorAll(selector)
    this.onClick()
  }

  /**
   * Click Event Listener on accordion classes
   */
  onClick () {
    this.accordions.forEach((acc) => {
      acc.addEventListener('click', () => {
        acc.classList.toggle('active')
        this.panel = acc.nextElementSibling
        if (this.panel.style.maxHeight) {
          this.panel.style.maxHeight = null
        } else {
          this.panel.style.maxHeight = this.panel.scrollHeight + 'px'
        }
      })
    })
  }
}

new Accordion('.accordion')
