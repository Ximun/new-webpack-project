import $ from 'jquery'
import 'magnific-popup'
import 'magnific-popup/dist/magnific-popup.css'

// Display youtube videos gallery using magnific-popup.js
$(function () {
    $('.video-element').each(function () {
        // $(this).append('<img src="https://img.youtube.com/vi/' + $(this).attr('video-id') + '/maxresdefault.jpg"/>')
        var id = $(this).attr('video-id')
        $(this).height($(this).width() / 1.78)
        $(this).css('background-image', 'url(https://img.youtube.com/vi/' + id + '/maxresdefault.jpg)')
        $(this).append('<img class="youtube-play" src="../img/youtube-play.png"/>')
        $(this).magnificPopup({
            items: [
                {
                    src: 'https://www.youtube.com/watch?v=' + id,
                    type: 'iframe'
                }
            ]
        })
    })
})
