# New Webpack Project #

A repository to easily start a blank webpack project and save my basic configuration.

`npm run dev` will start webpack webserver with hot reload on http://localhost:8080
`npm run prod` will compile EJS templates in html files inside `dist/app/`

The main js entry point is in `assets/js/app.js`
The main scss entry point is in `assets/scss/app.scss`

## Include EJS ##

To easily include files and variables in templating process, I use EJS. The main file is `/index.ejs`. The folder named `partials` contains all templating includes.

## Little personnal front-end framework ##

Little by litte, i will store in this repository the scss mixins and javascript function I usally use when i'm developping front-end mock-ups. Use them if you want!

### Using Modules

If you want to use a module, don't forget to uncomment the corresponding `dynamicImport()` line inside `assets/js/dynamicImport.js` and `assets/scss/app.scss`

#### JS selector
Click a category button and display the corresponding elements. Use it:

```html
<div id="selector">
    <div class="buttons">
        <button type="button" name="button" target="all">All</button>
        <button type="button" name="button" target="film">Films</button>
        <button type="button" name="button" target="book">Books</button>
    </div>
    <div class="objects">
        <div class="object film book">Lord of The Rings</div>
        <div class="object film">The Meaning of Life</div>
        <div class="object book">Ellana</div>
    </div>

</div>
```

#### Videos
Create a thumbnail youtube videos list, and open a youtube player by clicking on it, using [Magnific Popup JS](https://dimsemenov.com/plugins/magnific-popup/)

```html
<div class="videos-list">
    <div class="video-element" video-id="HpS84FZPbMs"></div>
    <div class="video-element" video-id="1FV4dp9ZaNA"></div>
    <div class="video-element" video-id="d5NsHRYex00"></div>
    <div class="video-element" video-id="COzyx-vweo8"></div>
    <div class="video-element" video-id="HS0Fvi7__bA"></div>
    <div class="video-element" video-id="RPUvuLh60j4"></div>
</div>
```

#### Accordion
```html
<div class="container">
    <h2 class="accordion">Accordion 1</h2>
    <p class="panel">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <h2 class="accordion">Accordion 2</h2>
    <p class="panel">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
</div>
```

### Thanks to Grafikart ###

I began to learn Webpack on https://www.grafikart.fr, so my config is inspired by him! Thank you very much!
